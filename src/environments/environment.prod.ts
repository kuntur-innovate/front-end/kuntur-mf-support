export const environment = {
  production: true,
  HOST:'https://jcvi4byieh.execute-api.us-east-1.amazonaws.com/stage/default',
  URL_BASE: 'https://dfbm1hega3d6y.cloudfront.net',
  URL_API_GATEWAY:'https://jcvi4byieh.execute-api.us-east-1.amazonaws.com/stage'
};
