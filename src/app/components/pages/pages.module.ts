import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/modules/material/material.module';
import { PageRoutingModule } from './pages.routing.module';
import { SupportComponent } from './support/support.component';
import { NewticketComponent } from './newticket/newticket.component';
import { NgBootstrapModule } from 'src/app/modules/ng-bootstrap/ng-bootstrap.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from '@angular/common/http';


// import { NombreComponenteComponent } from './nombre-componente/nombre-componente.component';

@NgModule({
  declarations: [SupportComponent,NewticketComponent],
  imports: [
    CommonModule, 
    MaterialModule, 
    PageRoutingModule,
    NgBootstrapModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ]
})
export class PagesModule { }
