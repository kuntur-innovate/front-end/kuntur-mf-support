import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SupportComponent } from './support/support.component';
import { NewticketComponent } from './newticket/newticket.component';
// import { NombreComponenteComponent } from './nombre-componente/nombre-componente.component';

const routes: Routes = [
// {path:'prueba', component:NombreComponenteComponent},

{path:'ticket', component: NewticketComponent},
{path:'**', component: SupportComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRoutingModule { }