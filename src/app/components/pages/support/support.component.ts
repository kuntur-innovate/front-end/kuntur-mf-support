import { TicketdetailsComponent } from './../../ticketdetails/ticketdetails.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { NewTicket } from 'src/app/interfaces/newticket.interface';
import { SupportService } from 'src/app/services/support.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {
  ticketsList !: NewTicket[];
  dataSource1: any;
  displayedColumns: string[] = ["code", "numberTicket", "subject", "description", "date", "service", "status"];

  constructor(private service: SupportService, private dialog: MatDialog) { 
  //  this.getlist();
   console.log(this.dataSource1);
  }

  // async getlist(){
  //   this.service.GetTickets().subscribe(res => {
  //     this.ticketsList = res;
  //     this.dataSource1 = new MatTableDataSource<NewTicket>(this.ticketsList);
  //     // this.dataSource.paginator = this.paginatior;
  //     // this.dataSource.sort = this.sort;
  //     console.log(this.dataSource1);
  //   });
  // }
  openDialog() {
    this.dialog.open(TicketdetailsComponent);
  }
  ngOnInit(): void {
  }
  
}


