import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import { SupportService } from '../../../services/support.service';
import { DOCUMENT } from '@angular/common';
import { NewTicket } from 'src/app/interfaces/newticket.interface';
import { first } from 'rxjs';

@Component({
  selector: 'app-newticket',
  templateUrl: './newticket.component.html',
  styleUrls: ['./newticket.component.scss']
})
export class NewticketComponent implements OnInit {
  public newTicket: FormGroup;
  public data;
  public dataNewTicket;
  public buttonClicked: boolean = false;
  public variable;

  // ticketForm!: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private supportService: SupportService,
    @Inject(DOCUMENT) private document: Document
  ) {
    
   }
  ngOnInit(){
    this.newTicket = new FormGroup({
      userName: new FormControl(''),
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      id: new FormControl('', [Validators.required, Validators.email]),
      subject: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      })
  }
  
  onSubmit() {
    if (this.newTicket.valid) {
      console.log(this.newTicket.value);
      this.dataNewTicket = JSON.stringify(this.newTicket.value);
      this.supportService.registrerTicket(this.newTicket);
      // Aquí puedes enviar los datos a través del servicio de soporte
    } else {
      console.log("Formulario inválido. Por favor, completa todos los campos.");
    }
    this.buttonClicked = true;
    console.log(typeof this.newTicket);
  }

  isButtonDisabled() {
    return !this.newTicket.valid && this.buttonClicked;
  }
  
}


