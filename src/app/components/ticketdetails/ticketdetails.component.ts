import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'uni-ticketdetails',
  templateUrl: './ticketdetails.component.html',
  styleUrls: ['./ticketdetails.component.scss']
})
export class TicketdetailsComponent implements OnInit {

  constructor(private ref:MatDialogRef<TicketdetailsComponent>) {}
  closepopup() {
    this.ref.close('Closed using function');
  }

  ngOnInit(): void {
  }

}
