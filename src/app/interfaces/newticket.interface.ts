
export interface NewTicket{
    code?:number;
    numberTicket?:string;
    userName?: string;
    firstName?: string;
    lastName?: string;
    id: string;
    subject:string;
    description: string;
    service?:string;
    date?:string;
    status?:string;
    user?:string;
}