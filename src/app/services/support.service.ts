import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { NewTicket } from '../interfaces/newticket.interface';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  private url = `${environment.URL_API_GATEWAY}`;


  constructor(
    private httpClient: HttpClient
  ) { }


  public getMessage(numessage:number): Observable<any> {
    let params: HttpParams = new HttpParams()
    .set('nmessage', numessage)
    return this.httpClient.post<any>(`${this.url}?${params}`,{})
  }
  registrerTicket(newticket: any): Observable<any> {
    return this.httpClient.post(`${this.url}/ticket`, newticket);
  }
  // GetTickets():Observable<NewTicket[]>{
  //   return this.httpClient.get<NewTicket[]>("http://localhost:5500/kuntur-mf-support/dbase.json");
  // }



}
